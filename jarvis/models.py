from django.db import models
from django.contrib.auth.models import User


class BaseModel(models.Model):
    """
    Generic abstract base class for all other models

    Fields:
    * :attr: `created_date` Creation date
    * :attr: `modified_date` Modified date
    * :attr: `deleted_date` Deletion date
    """
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True, null=True)
    deleted_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True


class Group(BaseModel):
    """"
    Model for storing Group details from Flickr
    
    Arguments:
        BaseModel {models.Model} -- Inherited Base Model
    """
    # Primary Key
    id = models.BigIntegerField(primary_key=True, editable=False)
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-created_date', )


class Owner(BaseModel):
    """
    Model for storing Owner details from Flickr

    Arguments:
        BaseModel {models.Model} -- Inherited BaseModel 
    """
    # Foreign Keys
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    # Primary Key
    nsid = models.CharField(max_length=250, primary_key=True)
    # API Key
    api_key = models.CharField(blank=True, max_length=255)
    # API Secret
    api_secret = models.CharField(blank=True, max_length=255)

    # Others
    real_name = models.CharField(max_length=100, blank=True, null=True)
    location = models.CharField(max_length=100, blank=True, null=True)
    path_alias = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.owner.username

    class Meta:
        ordering = ('-created_date', )


class Photo(BaseModel):
    """
    Model for storing Photo details from Flickr

    Arguments:
        BaseModel {models.Model} -- Inherited BaseModel 
    """

    #  Choices
    MEDIA_TYPES = (
        ('P', 'Photo'), 
        ('V', 'Video'),
    )

    # Primary Key
    id = models.BigIntegerField(primary_key=True, editable=False, help_text="Unique ID for Photo")
    # Foreign Keys
    owner = models.ForeignKey(Owner, on_delete=models.CASCADE)

    # Others    
    title = models.CharField(max_length=1000, blank=True, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True)

    type = models.CharField(max_length=2, choices=MEDIA_TYPES)
    secret = models.CharField(max_length=50)
    views_count = models.PositiveIntegerField()
    comments_count = models.PositiveIntegerField()
    url = models.URLField()
    date_uploaded = models.DateTimeField()
    date_posted = models.DateTimeField()
    date_taken = models.DateTimeField()
    date_updated = models.DateTimeField()

    # Boolean Fields
    is_public = models.NullBooleanField()
    is_friend = models.NullBooleanField()
    is_family = models.NullBooleanField()

    def __str__(self):
        return self.id

    class Meta:
        ordering = ('-created_date', )




