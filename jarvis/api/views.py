# Internal Imports
from django.contrib.auth import authenticate, logout
# External Imports
from rest_framework.views import View
from rest_framework.authtoken.models import Token
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from django.http import JsonResponse


class Login(View):
    def post(self, request):
        username = request.POST.get("username")
        password = request.POST.get("password")
        if username is None or password is None:
            return Response({'error': 'Please provide both username and password'}, status=HTTP_400_BAD_REQUEST)
        user = authenticate(username=username, password=password)
        if not user:
            return Response({'error': 'Invalid Credentials'}, status=HTTP_404_NOT_FOUND)
        token, _ = Token.objects.get_or_create(user=user)
        return JsonResponse({'token': token.key}, status=HTTP_200_OK)


class Logout(View):
    def get(self, request):
        logout(request)
        return JsonResponse({'response': 'Logged Out.'}, status=HTTP_200_OK)