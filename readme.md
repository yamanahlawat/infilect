# Infilect

### Project Description:
  Infilect

- Technology:
    - Back-End:
         - Python 3.6
         - Django Framework 2.1.


### Installation:
- creating virutal environment for the project - using virtualenvwrapper.
  - run the command: `mkvirtualenv infilect`
- activating virtual environment:
    - run the command: `workon infilect`
- installing dependencies:
    - run the command from the project folder:
        - `pip install -r requirements/base.txt`
- Source the environment file:
        - `source infilect.env`
- run the server:
    - run the command: `python manage.py runserver` 
    - default port to access website: http://127.0.0.1:8000/
 
